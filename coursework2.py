"""def calculate_c(subjects):
    total_credit_points = 0
    total_credits = 0

    for credit, grade in subjects:
        credit_points = credit * grade
        total_credit_points += credit_points
        total_credits += credit

    g = total_credit_points / total_credits
    return g

subjects_data = [
    (4, 3.5),
    (3, 4.0),
    (3, 3.0),
    (4, 4.0),
]
G1 = calculate_c(subjects_data)

file_name = 'report.txt'
with open(file_name, 'w') as file:
    file.write(f"Calculated : {G1:.2f}")

print(f"written to {file_name} successfully.")"""

def calculate_c(subjects):
    total_credit_points = 0
    total_credits = 0

    for credit, grade in subjects:
        credit_points = credit * grade
        total_credit_points += credit_points
        total_credits += credit

    g = total_credit_points / total_credits
    return g

with open('subjects.txt', 'w+') as file:
    subjects_data = [tuple(map(float, line.split())) for line in file]

G1 = calculate_c(subjects_data)

with open('report.txt', 'w') as file:
    file.write(f"Calculated : {G1:.2f}")
print(f"written to report.txt successfully.")
#from math
"""class Mathsfunctions():
    def add(a,b):
        return a+b
    def subtract(a,b):
        return a-b
    def multiply(a,b):
        return a*b
    def divide(a,b):
        if b!=0:
            return a/b
        else:
            return "Division by zero is impossible"
class""" 


